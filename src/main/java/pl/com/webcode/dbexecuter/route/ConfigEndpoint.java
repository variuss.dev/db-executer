package pl.com.webcode.dbexecuter.route;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.com.webcode.dbexecuter.model.TestData;
import pl.com.webcode.dbexecuter.repository.DummyRepo;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest")
@RequiredArgsConstructor
public class ConfigEndpoint {

    private final DummyRepo dummyRepo;

    @GetMapping
    public List<TestData> testDataList() {
        List<TestData> result = new ArrayList<>();
        dummyRepo.findAll().forEach(result::add);
        return result;
    }
}
