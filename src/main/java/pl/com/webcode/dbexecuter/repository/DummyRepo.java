package pl.com.webcode.dbexecuter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.com.webcode.dbexecuter.model.TestData;

@Repository
public interface DummyRepo extends CrudRepository<TestData, Long> {
}
