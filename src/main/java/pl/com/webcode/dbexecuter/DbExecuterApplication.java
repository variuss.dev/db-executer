package pl.com.webcode.dbexecuter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbExecuterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbExecuterApplication.class, args);
	}

}
